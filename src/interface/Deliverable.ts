import { log } from "console";

interface Deliverable{
    estimateDeliveryTime(weight: number): number;
    calculateShippingFee(weight: number): number;
}

export { Deliverable };