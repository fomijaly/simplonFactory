class Product {
  productId: number;
  name: string;
  weight: number;
  price: number;
  dimension: Dimension;

  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimension: Dimension
  ) {
    this.productId = productId;
    this.name = name;
    this.weight = weight;
    this.price = price;
    this.dimension = dimension;
  }

  displayDetails(): string {
    return `\nProduct ID: ${this.productId}, Name: ${this.name}, \nWeight: ${this.weight}, Longueur: ${this.dimension.length}, Largeur: ${this.dimension.width}, Hauteur: ${this.dimension.height}, \nPrice: ${this.price} €`;
  }
}

type Dimension = {
  length: number;
  width: number;
  height: number;
};


export { Product, Dimension };
