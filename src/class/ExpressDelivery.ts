import { Deliverable } from "../interface/Deliverable";

class ExpressDelivery implements Deliverable{
    estimateDeliveryTime(weight: number): number{
        return weight <= 5 ? 1 : 3;
    }
    calculateShippingFee(weight: number): number {
        if(weight < 1){
            return 8;
        } else if(weight < 5){
            return 14
        } else{
            return 30
        }
    }
}

export {ExpressDelivery}