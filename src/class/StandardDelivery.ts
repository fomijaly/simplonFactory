import { Deliverable } from "../interface/Deliverable";

class StandardDelivery implements Deliverable{
    estimateDeliveryTime(weight: number): number{
        return weight < 10 ?  7 : 10;
    }
    calculateShippingFee(weight: number): number {
        if(weight < 1){
            return 5;
        } else if(weight < 5){
            return 10;
        } else{
            return 20;
        } 
    }
}

export { StandardDelivery }