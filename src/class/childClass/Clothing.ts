import { Product, Dimension } from '../Product';

enum ClothingSize {
  XS = 'XS',
  S = 'S',
  M = 'M',
  L = 'L',
  XL = 'XL',
  XXL = 'XXL'
}

class Clothing extends Product {
  size: ClothingSize;

  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimension: Dimension,
    size: ClothingSize
  ) {
    super(productId, name, weight, price, dimension);
    this.size = size;
  }

  displayDetails(): string {
    return super.displayDetails() + `\nSize: ${this.size}`;
  }
}

export { Clothing, ClothingSize };
