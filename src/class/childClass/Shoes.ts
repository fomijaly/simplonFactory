import { Product, Dimension } from '../Product';

enum ShoeSize {
  shoeSize36 = 36,
  shoeSize37,
  shoeSize38,
  shoeSize39,
  shoeSize40,
  shoeSize41,
  shoeSize42,
  shoeSize43,
  shoeSize44,
  shoeSize45,
  shoeSize46
}

class Shoes extends Product {
  shoes: ShoeSize;

  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimension: Dimension,
    shoes: ShoeSize
  ) {
    super(productId, name, weight, price, dimension);
    this.shoes = shoes;
  }

  displayDetails(): string {
    return super.displayDetails()+ `\nShoes: ${this.shoes}`
  }
}

export { Shoes , ShoeSize };
