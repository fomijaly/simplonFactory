import { Customer } from './Customer';
import { Product } from './Product';
import { Deliverable } from '../interface/Deliverable';

class Order {
  orderId: number;
  customer: Customer;
  productsList: Product[];
  orderDate: Date;
  delivery: Deliverable | undefined;

  constructor(
    orderId: number,
    customer: Customer,
    productsList: Product[],
    orderDate: Date,
  ) {
    this.orderId = orderId;
    this.customer = customer;
    this.productsList = productsList;
    this.orderDate = orderDate;
  }

  addProduct(product: Product){
    this.productsList.push(product)
  }

  removeProduct(productId: number){
    this.productsList.splice(productId, 1)
  }

  calculateWeight(){
    const initialWeight = 0;
    return this.productsList.map((productWeight) => productWeight.weight).reduce((accumulator, currentValue) => accumulator + currentValue, initialWeight);
  }

  calculateTotal(){
    const initialPrice = 0;
    return this.productsList.map((productPrice) => productPrice.price).reduce((accumulator, currentValue) => accumulator + currentValue, initialPrice);
  }

  displayOrder(){
    return `Le client: ${this.customer.displayInfo()} \nLes commandes: ${this.productsList.map((eachOrder) => eachOrder.displayDetails())} \nTotal : ${this.calculateTotal()}€`
  }

  setDelivery(delivery: Deliverable){
    this.delivery = delivery;
  }

  calculateShippingCost(){
    return this.delivery? this.delivery.calculateShippingFee(this.calculateWeight()) : 0;
  }

  estimateDeliveryTime(){
    return this.delivery?.estimateDeliveryTime(this.calculateWeight())
  }
}

export { Order };
