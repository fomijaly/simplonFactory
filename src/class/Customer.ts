class Customer {
  customerId: number;
  name: string;
  email: string;
  address: Address | undefined;

  constructor(customerId: number, name: string, email: string) {
    this.customerId = customerId;
    this.name = name;
    this.email = email;
  }

  displayInfo(): string {
    return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, ${this.displayAddress()}`;
  }

  setAddress(address: Address) {
    this.address = address;
  }

  displayAddress(): string {
    return this.address
      ? `Address: ${this.address.street}, ${this.address.city}, ${this.address.zipCode} ${this.address.country.toUpperCase()}`
      : 'No address found';
  }
}

type Address = {
  street: string;
  city: string;
  zipCode: string;
  country: string;
};

export { Customer };
