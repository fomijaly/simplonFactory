import { log } from 'console';
import { Customer } from './class/Customer';
import { Product } from './class/Product';
import { ClothingSize, Clothing } from './class/childClass/Clothing';
import { ShoeSize, Shoes } from './class/childClass/Shoes';
import { Order } from './class/Order';
import { StandardDelivery } from './class/StandardDelivery';

const product1 = new Product(1, 'Basket Nike', 20, 45, {
  length: 1,
  width: 3,
  height: 5
});
const firstProduct = product1.displayDetails();

const customer1 = new Customer(1, 'Antoine', 'a.ch@yahoo.com');
const customer2 = new Customer(2, 'Marine', 'm.th@yahoo.com');
customer1.setAddress({
  street: 'Avenue JC',
  city: 'Toulouse',
  zipCode: '31500',
  country: 'France'
});

// log(firstProduct);
// log(customer1.displayInfo())
// log(customer2.displayInfo())

const cloth1 = new Clothing(
  1,
  'Manteau',
  800,
  450,
  { length: 1, width: 3, height: 5 },
  ClothingSize.L
);
const shoes1 = new Shoes(
  1,
  'Puma',
  450,
  90,
  { length: 30, width: 25, height: 15 },
  ShoeSize.shoeSize38
);
// log(cloth1)
// log(cloth1.displayDetails())
// log(shoes1)
// log(shoes1.displayDetails())

const newArray: Product[] = [];
const newDate: Date = new Date(2024, 2, 4);
const firstOrder = new Order(1, customer1, newArray, newDate);

firstOrder.addProduct(product1);
firstOrder.addProduct(cloth1);
// log(firstOrder);
log(firstOrder.displayOrder());

const secondArray: Product[] = [];
const secondDate: Date = new Date(2023, 8, 10);
const secondOrder = new Order(2, customer2, secondArray, secondDate);

secondOrder.addProduct(shoes1);
// log(secondOrder);
log(secondOrder.displayOrder());

const newDelivery = new StandardDelivery();
secondOrder.setDelivery(newDelivery);

log('Frais de livraison: ', secondOrder.calculateShippingCost() + "€");
log('Estimation délai de livraison: ', secondOrder.estimateDeliveryTime() + "jours")