# Installation du projet
Initialiser le projet avec les dépendances actuelles. 

```bash
npm ci
``` 

## Lancer les scripts
Lancer Nodemon
```bash 
npm run dev
```

Lancer le dossier dist
```bash 
npm start
```

Transpiler le projet 
```bash 
npm run build
```

# Objectifs du projet
- Créer un système qui gère :
    - les commandes des clients,
    - le catalogue de produits,
    - le processus de livraison.

Ce système devra permettre de :

- créer et de gérer des clients (Customer),
- de gérer un inventaire de produits (Product) avec des spécificités telles que:
    - les vêtements (Clothing)
    - les chaussures (Shoes),
- de gérer des commandes (Order) avec une logique de livraison flexible (Deliverable).